import Queue from './modules/Queue';

declare global {
  namespace Express {
    interface Request {
      queue?: Queue;
      token?: string | string[] | null;
    }
  }
}
