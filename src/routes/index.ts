import express from 'express';
import fileRoutes from './converter';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('Hello!');
});

router.get('/health', (req, res) => {
  res.send('OK');
});

router.use(fileRoutes);

export default router;
