import express, { NextFunction, Request, Response } from 'express';
import multer from 'multer';
import { MAX_FILE_SIZE, UPLOAD_DIR } from '../constants';
import path from 'node:path';
import { createHashedToken } from '../utils/utils';

const router = express.Router();
const upload = multer({
  dest: UPLOAD_DIR,
  limits: {
    fileSize: MAX_FILE_SIZE,
  },
});

router.post(
  '/files/upload',
  upload.single('file'),
  async (req: Request, res: Response, next: NextFunction) => {
    if (req.queue && req.file) {
      const fileToken = await createHashedToken();
      req.token = fileToken;
      const outputPath = path.parse(req.file.path);
      const file = path.format({
        root: outputPath.root,
        name: outputPath.name,
        ext: '.' + req.body.format,
      });

      req.queue.add({
        filePath: req.file.path,
        outputPath: file,
        userId: req.sessionID,
        name: req.file.filename,
        fileId: fileToken,
        format: req.body.format,
      });
    }
    next();
  },
  async (req: Request, res: Response) => {
    const file = req.file;

    if (!file) {
      res.json({ error: 'File not found.' });
    }
    if (file!.size > MAX_FILE_SIZE) {
      res.json({ error: 'File is larger then 5MB.' });
    }

    req.token
      ? res.json({ token: req.token })
      : res.json({
          error: 'Something went wrong.',
        });
  }
);

router.get('/files/:token', async (req: Request, res: Response) => {
  const result = await req.queue?.getFileByToken(req.params.token);
  res.json(result ? result : { status: 'NOTFOUND' });
});

export default router;
