import crypto from 'crypto';
import { unlink } from 'fs/promises';

export async function createHashedToken() {
  return crypto.randomBytes(16).toString('hex');
}

export async function removeFile(path: string) {
  try {
    await unlink(path);
  } catch (error) {
    console.log(`Unlink error: ${(error as Error).message}`);
  }
}
