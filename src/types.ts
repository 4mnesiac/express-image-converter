import { AvailableFormatInfo, FormatEnum } from 'sharp';

export enum Statuses {
  Created = 'Created',
  Waiting = 'Waiting',
  Processing = 'Processing',
  Success = 'Success',
  Error = 'Error',
}
export type Task = {
  filePath: string;
  outputPath: string;
  userId: string;
  fileId: string;
  name?: string;
  interval?: number;
  format: keyof FormatEnum | AvailableFormatInfo;
  status?: Statuses;

  [k: string]: unknown;
};
