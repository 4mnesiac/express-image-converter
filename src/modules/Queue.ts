import { NextFunction, Request, Response } from 'express';
import { Statuses, Task } from '../types';
import { createHashedToken } from '../utils/utils';

export type Executor<T = unknown> = (task: Task) => Promise<T>;
export type TResultItem = {
  file: unknown;
  status: Statuses;
};
export type Result = {
  [key: string]: TResultItem;
};

export default class Queue {
  public processes: number;
  public tasks: Task[];
  public results: Result;
  private onProcess: Executor | null;
  public concurrency: number;

  constructor(concurrency: number) {
    this.tasks = [];
    this.results = {};
    this.concurrency = concurrency;
    this.processes = 0;
    this.onProcess = null;
  }

  static init(concurrency: number) {
    return new Queue(concurrency);
  }

  add(item: Task) {
    const hasChannel = this.processes < this.concurrency;
    const task = {
      ...item,
      token: createHashedToken(),
      format: item.format ?? 'webp',
      status: Statuses.Waiting,
    };

    if (hasChannel) {
      this.next(task);
      return;
    }
    this.tasks.push(task);
  }

  use() {
    return (req: Request, res: Response, next: NextFunction) => {
      req.queue = this;
      next();
    };
  }

  private async next(item: Task) {
    this.processes++;
    item.status = Statuses.Processing;
    if (this.onProcess) {
      while (this.processes) {
        try {
          const result = await this.onProcess(item);
          if (result) {
            item.status = Statuses.Success;
            this.results[item.fileId] = {
              file: result,
              status: Statuses.Success,
            };
          }

          this.processes--;
          if (this.tasks.length > 0) {
            const task = this.tasks.shift()!;
            this.next(task);
          }
        } catch (error) {
          item.status = Statuses.Error;
          console.error({ error });
        }
      }
    }
  }
  async getFileByToken(token: string): Promise<TResultItem | undefined> {
    return this.results[token];
  }

  executor<T>(listener: Executor<T>) {
    this.onProcess = listener;
    return this;
  }
}
