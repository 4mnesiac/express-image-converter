import sharp from 'sharp';
import { Task } from '../types';
import { UPLOAD_DIR } from '../constants';
import path from 'path';
import { removeFile } from '../utils/utils';

export const sharpConverter = async ({
  filePath,
  outputPath,
  format,
}: Task) => {
  const file = await sharp(filePath)
    .toFormat(format)
    .toFile(path.join(UPLOAD_DIR, outputPath));
  removeFile(filePath);
  return file;
};
