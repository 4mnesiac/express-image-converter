import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import router from './routes';
import session from 'express-session';
import dotEnvExtended from 'dotenv-extended';
import { sharpConverter } from './modules/Converter';
import Queue from './modules/Queue';
import sharp from 'sharp';

dotEnvExtended.load();
const { PORT = 3000, NODE_ENV = 'dev', SESSION_SECRET } = process.env;

const app = express();
const queue = Queue.init(3).executor<sharp.OutputInfo>(sharpConverter).use();

app.use(cors());
app.use(
  session({
    secret: SESSION_SECRET || '055d5e5d2d33f5c9b6b7cd050c8330',
    resave: true,
    saveUninitialized: true,
  })
);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(queue);
app.use(router);
app.use(express.static('upload'));
app.use(morgan(NODE_ENV));

app.listen(PORT, () => {
  console.log(`Example app listening on port ${PORT}`);
});
